from django.test import TestCase
from .models import UnidadAcademica


class TestUnidadAcademica(TestCase):

    def test_agrega_unidad(self):
        UnidadAcademica.objects.create(
            nombre='UAIE',
            descripcion='La Unidad Académica más chida'
        )
        self.assertEqual(UnidadAcademica.objects.count(), 1)
