from django.urls import path
from . import views

# /unidades/lista
urlpatterns = [
    path('lista/', views.lista_programas, name='lista_programas'),
    path('nuevo/', views.nuevo_programa, name='nuevo_programa'),
    # path('eliminar/<int:id>', views.eliminar_unidad, name='eliminar_unidad'),
    # path('editar/<int:id>', views.editar_unidad, name='editar_unidad'),
]
