En Windows:
    Instalar en chocolatey:
    https://chocolatey.org/install


    Instalar Kind:
    choco install kind

Crear cluster:
    kind create cluster --config cluster.yml

Para crear el ingress de nginx
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml


Crear namespace:
    kubectl create namespace produccion